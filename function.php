<?php

function get_entete() {
    require './entete.html';
}


function appeldepage($nomPages) 
{
  switch($nomPages) {
    case 'exercice1' : require 'exercice1.php'; break; 
    case 'exercice2' : require 'exercice2.php'; break;
    case 'exercice3' : require 'exercice3.php'; break;
    case 'exercice4' : require 'exercice4.php'; break;
    case 'exercice5' : require 'exercice5.php'; break;
    case 'exercice6' : require 'exercice6.php'; break;
    case 'exercice7' : require 'exercice7.php'; break;
    default : require 'index.php'; break; 
}
}
?>